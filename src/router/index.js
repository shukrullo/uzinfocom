import { createRouter, createWebHistory, RouterView } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import projects from '../views/projects.vue'
import news_list from '../views/news-list.vue'
import leader from '../views/leader.vue'
import i18n from '@/i18n'
import project_detail from '../views/project-detail.vue'
import detail from '../views/detail.vue'
import news_detail from '../views/news-detail.vue'
import license from '../views/license.vue'
import tender from '../views/tender.vue'
import contact from '../views/contact.vue'
import vacancy from '../views/vacancy.vue'
import vacancy_detail from '../views/vacancy-detail.vue'
import search from  '../views/search.vue'
import help from "../views/help.vue"
import photogallery from "../views/Photogallery.vue";
import videogallery from "../views/Videogallery.vue";

const routes = [
  {
    path: '',
    redirect: `/${i18n.global.locale}`,
  },
  {
    path: '/:lang',
    component: RouterView,
    children: [
      {
        path: '',
        name: 'home',
        component: HomeView,
      },
      {
        path: 'about/:id',
        name: 'about',
        component: AboutView,
      },
      {
        path: 'projects/:id',
        name: 'projects',
        component: projects,
      },
      {
        path: 'news-list/:id',
        name: 'news-list',
        component: news_list,
      },
      {
        path: 'leader/:id',
        name: 'leader',
        component: leader,
      },
      {
        path: 'project-detail/:id',
        name: 'project-detail',
        component: project_detail,
      },
      {
        path: 'detail/:id',
        name: 'detail',
        component: detail,
      },
      {
        path: 'news-detail/:id',
        name: 'news-detail',
        component: news_detail,
      },
      {
        path: 'license/:id',
        name: 'license',
        component: license,
      },
      {
        path: 'tender/:id',
        name: 'tender',
        component: tender,
      },
      {
        path: 'contact',
        name: 'contact',
        component: contact,
      },
      {
        path: 'vacancy',
        name: 'vacancy',
        component: vacancy,
      },
      {
        path: 'vacancy-detail/:id',
        name: 'vacancy-detail',
        component: vacancy_detail,
      },
      {
        path: 'search',
        name: 'search',
        component: search,
      },
      {
        path: 'help',
        name: 'help',
        component: help,
      },
      {
        path: 'photogallery/:id',
        name: 'photogallery',
        component: photogallery,
      },
      {
        path: 'videogallery/:id',
        name: 'videogallery',
        component: videogallery,
      },
    ],
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior() {
    window.scrollTo(0, 0, { behavior: 'smooth' });
  },
})

router.beforeEach((to, from, next) => {
  i18n.global.locale = to.params.lang;
  next();
})

export default router
