import { createStore } from 'vuex'
import createPersistedState from 'vuex-persistedstate'

export default createStore({
  state: {
    messages: Array(),
    menus: Array(),
  },
  getters: {
  },
  mutations: {
    messages_mutation(state, message) {
      state.messages = message;
    },
    menus_mutation(state, menus) {
      state.menus = menus;
    },
  },
  actions: {
    messages_action({ commit }, message) {
      commit('messages_mutation', message);
    },
    menus_action({ commit }, menus) {
      commit('menus_mutation', menus);
    },
  },
  modules: {
  },
  plugins: [createPersistedState()],
})
