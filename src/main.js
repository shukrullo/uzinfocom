import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './i18n'

let app = createApp(App);

function translate(params) {
    const message = store.state.messages.filter((detail) => detail.key == params);
    return message.length > 0 ? message[0].message : params;
}

app.config.globalProperties.$translate = translate;

app.use(i18n).use(store).use(router).mount('#app')
